package com.dagnetdamien.poqchallenge.presentation

import android.view.View
import com.dagnetdamien.poqchallenge.domain.GithubRepoModel
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SquareRepoListViewModelMapperTest {

    private val mapper = SquareRepoListViewModelMapper()

    private val retrofitGithubRepoListViewModelItem = SquareRepoListViewModel.Item(
        name = "Retrofit",
        description = "http client",
        stars = "3",
        watchers = "5",
        forks = "10",
        url = "www.github.com",
        ownerName = "square",
        ownerImageUrl = "square.com",
        licenseName = "MIT LICENCE"
    )

    private val retrofitGithubRepoModel = GithubRepoModel(
        name = retrofitGithubRepoListViewModelItem.name,
        description = retrofitGithubRepoListViewModelItem.description,
        stargazersCount = 3,
        forksCount = 10,
        watchersCount = 5,
        ownerName = retrofitGithubRepoListViewModelItem.ownerName,
        ownerImageUrl = retrofitGithubRepoListViewModelItem.ownerImageUrl,
        licenseName = retrofitGithubRepoListViewModelItem.licenseName,
        url = retrofitGithubRepoListViewModelItem.url
    )

    @Test
    fun Should_MapGithubRepoModelIntoSquareRepoListViewModel() {
        val expectedViewModel = SquareRepoListViewModel(
            header = SquareRepoListViewModel.Header(
                title = retrofitGithubRepoListViewModelItem.ownerName,
                imageUrl = retrofitGithubRepoListViewModelItem.ownerImageUrl,
                visibility = View.VISIBLE
            ),
            repoList = listOf(retrofitGithubRepoListViewModelItem)
        )

        val actualViewModel = mapper.toSquareRepoListViewModel(listOf(retrofitGithubRepoModel))
        assertThat(actualViewModel).isEqualTo(expectedViewModel)
    }
}
package com.dagnetdamien.poqchallenge.presentation

import com.dagnetdamien.poqchallenge.domain.GetSquareGithubReposUseCase
import com.dagnetdamien.poqchallenge.domain.GithubRepoModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SquareRepoListPresenterTest {

    @Mock
    lateinit var getSquareGithubReposUseCase: GetSquareGithubReposUseCase

    @Mock
    lateinit var squareRepoListViewModelMapper: SquareRepoListViewModelMapper

    @Mock
    lateinit var view: SquareRepoListContract.View

    lateinit var presenter: SquareRepoListPresenter

    @Before
    fun setUp() {
        Dispatchers.setMain(Dispatchers.Unconfined)
        presenter = spy(
            SquareRepoListPresenter(
                getSquareGithubReposUseCase = getSquareGithubReposUseCase,
                squareRepoListViewModelMapper = squareRepoListViewModelMapper
            )
        )
        presenter.setView(view)
    }

    @Test
    fun Should_ShowSquareRepoListInView() {
        val githubRepoModels = listOf(GithubRepoModel())
        runBlocking {
            doReturn(githubRepoModels).`when`(getSquareGithubReposUseCase).get()
        }

        val squareRepoListViewModel = SquareRepoListViewModel(
            SquareRepoListViewModel.Header(),
            listOf(mockSquareRepoListItemViewModel())
        )
        doReturn(squareRepoListViewModel)
            .`when`(squareRepoListViewModelMapper)
            .toSquareRepoListViewModel(githubRepoModels)

        presenter.initialise()

        verify(view).showLoading()
        verify(view).showSquareRepoListInView(squareRepoListViewModel)
        verify(view).hideLoading()
    }

    @Test
    fun Should_ShowAnError() {
        val exception = Exception()

        runBlocking {
            doAnswer { throw exception }.`when`(getSquareGithubReposUseCase).get()
        }
        doNothing().`when`(presenter).logError(exception)

        presenter.initialise()

        verify(view).showLoading()
        verify(view).showError()
        verify(view).hideLoading()
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    private fun mockSquareRepoListItemViewModel(): SquareRepoListViewModel.Item {
        return SquareRepoListViewModel.Item(
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        )
    }
}
package com.dagnetdamien.poqchallenge.data.database

import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LocalGithubReposDataStoreImplTest {

    @Mock
    lateinit var database: AppDatabase

    @Mock
    lateinit var githubRepoDao: GithubRepoDao

    lateinit var localGithubReposDataStore: LocalGithubReposDataStoreImpl

    @Before
    fun setUp() {
        localGithubReposDataStore = LocalGithubReposDataStoreImpl(database)

        doReturn(githubRepoDao).`when`(database).githubRepoDao()
    }

    @Test
    fun Should_ReturnAllGithubRepos() {
        runBlocking {
            val expectedRepos = listOf(GithubRepo("repo-id-43j2n"))
            doReturn(expectedRepos).`when`(githubRepoDao).getAll()

            val actualRepos = localGithubReposDataStore.findAllSquareRepos()

            assertThat(actualRepos).isEqualTo(expectedRepos)
        }
    }

    @Test
    fun Should_SaveAllGithubRepos() {
        runBlocking {
            val repoToSave = GithubRepo("repo-id-r32jn")

            localGithubReposDataStore.saveAll(listOf(repoToSave))

            verify(githubRepoDao).insertAll(repoToSave)
        }
    }
}

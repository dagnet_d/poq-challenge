package com.dagnetdamien.poqchallenge.data

import com.dagnetdamien.poqchallenge.data.database.GithubRepo
import com.dagnetdamien.poqchallenge.data.network.GithubRepoResponse
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doAnswer
import org.mockito.Mockito.doReturn
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GithubReposRepositoryImplTest {

    @Mock
    lateinit var localGithubReposDataStore: LocalGithubReposDataStore

    @Mock
    lateinit var networkGithubReposDataStore: NetworkGithubReposDataStore

    lateinit var githubReposRepository: GithubReposRepositoryImpl

    @Before
    fun setUp() {
        githubReposRepository = GithubReposRepositoryImpl(
            localGithubReposDataStore = localGithubReposDataStore,
            networkGithubReposDataStore = networkGithubReposDataStore
        )
    }

    @Test
    fun Should_ReturnGihubReposFromCache_When_CacheIsValid() {
        runBlocking {
            val cachedGithubRepos = listOf(GithubRepo("cache-repo"))

            doReturn(cachedGithubRepos).`when`(localGithubReposDataStore).findAllSquareRepos()

            val actualSquareRepos = githubReposRepository.squareRepos()

            assertThat(actualSquareRepos).isEqualTo(cachedGithubRepos)
        }
    }

    @Test
    fun Should_ReturnGihubReposFromNetwork_When_CacheIsNotValidAndFetchingIsSuccesfull() {
        runBlocking {
            doReturn(emptyList<GithubRepo>()).`when`(localGithubReposDataStore).findAllSquareRepos()

            val networkGithubRepoResponses = listOf(GithubRepoResponse("retrofit-repo"))
            doReturn(networkGithubRepoResponses).`when`(networkGithubReposDataStore).squareRepos()

            val expectedGithubRepos = listOf(GithubRepo("retrofit-repo"))
            val actualSquareRepos = githubReposRepository.squareRepos()

            assertThat(actualSquareRepos).isEqualTo(expectedGithubRepos)
        }
    }

    @Test(expected = SquareReposNotFoundException::class)
    fun Should_ThrowException_When_CacheIsNotValidAndFetchingIsSuccesfullAndNetworkResponseIsEmpty() {
        runBlocking {
            doReturn(emptyList<GithubRepo>())
                .`when`(localGithubReposDataStore)
                .findAllSquareRepos()

            doReturn(emptyList<GithubRepoResponse>())
                .`when`(networkGithubReposDataStore)
                .squareRepos()

            githubReposRepository.squareRepos()
        }
    }

    //    case: Missing response item ID
    @Test(expected = SquareReposNotFoundException::class)
    fun Should_ThrowException_When_CacheIsNotValidAndFetchedItemMappingFails() {
        runBlocking {
            doReturn(emptyList<GithubRepo>()).`when`(localGithubReposDataStore).findAllSquareRepos()

            val networkGithubRepoResponses = listOf(GithubRepoResponse())
            doReturn(networkGithubRepoResponses).`when`(networkGithubReposDataStore).squareRepos()

            githubReposRepository.squareRepos()
        }
    }

    @Test(expected = Exception::class)
    fun Should_ThrowException_When_CacheIsNotValidAndFetchingThrowException() {
        runBlocking {
            doReturn(emptyList<GithubRepo>()).`when`(localGithubReposDataStore).findAllSquareRepos()

            doAnswer { throw Exception() }.`when`(networkGithubReposDataStore).squareRepos()

            githubReposRepository.squareRepos()
        }
    }
}

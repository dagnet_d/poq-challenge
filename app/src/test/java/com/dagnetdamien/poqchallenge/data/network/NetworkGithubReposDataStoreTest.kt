package com.dagnetdamien.poqchallenge.data.network

import com.dagnetdamien.poqchallenge.data.NetworkGithubReposDataStore
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doAnswer
import org.mockito.Mockito.doReturn
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class NetworkGithubReposDataStoreTest {

    @Mock
    lateinit var githubApi: GithubApi

    lateinit var networkGithubReposDataStore: NetworkGithubReposDataStore

    @Before
    fun setUp() {
        networkGithubReposDataStore = NetworkGithubReposDataStoreImpl(githubApi)
    }

    @Test
    fun Should_ReturnGihubReposResponse_When_RequestIsSuccessful() {
        runBlocking {
            val expectedGithubReposResponse = listOf(GithubRepoResponse())
            doReturn(CompletableDeferred(expectedGithubReposResponse))
                .`when`(githubApi)
                .squareRepos()

            val actualGithubReposResponse = networkGithubReposDataStore.squareRepos()

            assertThat(actualGithubReposResponse).isEqualTo(expectedGithubReposResponse)
        }
    }

    @Test(expected = Exception::class)
    fun Should_ThrowAnError_When_RequestFails() {
        runBlocking {
            doAnswer { throw Exception() }
                .`when`(githubApi)
                .squareRepos()

            networkGithubReposDataStore.squareRepos()
        }
    }
}

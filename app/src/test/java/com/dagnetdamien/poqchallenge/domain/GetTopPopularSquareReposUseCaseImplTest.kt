package com.dagnetdamien.poqchallenge.domain

import com.dagnetdamien.poqchallenge.data.database.GithubRepo
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetTopPopularSquareReposUseCaseImplTest {

    @Mock
    lateinit var githubReposRepository: GithubReposRepository

    lateinit var useCase: GetTopPopularSquareReposUseCaseImpl

    @Before
    fun setUp() {
        useCase = spy(GetTopPopularSquareReposUseCaseImpl(githubReposRepository))
    }

    @Test
    fun Should_ReturnTopPopularSquareRepos_When_SquareReposCanBeRetrievedSuccesfully() {
        runBlocking {
            doReturn(2).`when`(useCase).topLimit()

            val githubRepos =
                listOf(mostWatchedGithubRepo, mostPopularGithubRepo, mostStarredGithubRepo)

            doReturn(githubRepos).`when`(githubReposRepository).squareRepos()

            val actualGithubRepoModels = useCase.get()

            assertThat(actualGithubRepoModels)
                .hasSize(2)
                .containsExactly(*expectedTopPopularSquareRepos.toTypedArray())
        }
    }

    @Test(expected = Exception::class)
    fun Should_ThrowException_When_SquareReposCannotBeRetrieved() {
        runBlocking {
            doAnswer { throw Exception() }.`when`(githubReposRepository).squareRepos()

            useCase.get()
        }
    }

    private val mostPopularGithubRepo = GithubRepo(
        id = "431ln",
        name = "most-popular-repo",
        watchersCount = 100,
        stargazersCount = 100,
        forksCount = 100
    )

    private val mostWatchedGithubRepo = GithubRepo(
        id = "53nj54",
        name = "most-watched-repo",
        watchersCount = 50,
        stargazersCount = 2,
        forksCount = 1
    )

    private val mostStarredGithubRepo = GithubRepo(
        id = "uh08ub",
        name = "most-starred-repo",
        watchersCount = 10,
        stargazersCount = 200,
        forksCount = 10
    )


    val expectedTopPopularSquareRepos = listOf(
        GithubRepoModel(
            name = mostPopularGithubRepo.name,
            watchersCount = mostPopularGithubRepo.watchersCount,
            stargazersCount = mostPopularGithubRepo.stargazersCount,
            forksCount = mostPopularGithubRepo.forksCount
        ),
        GithubRepoModel(
            name = mostStarredGithubRepo.name,
            watchersCount = mostStarredGithubRepo.watchersCount,
            stargazersCount = mostStarredGithubRepo.stargazersCount,
            forksCount = mostStarredGithubRepo.forksCount
        )
    )

}
package com.dagnetdamien.poqchallenge.presentation

import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class SquareRepoPresentationModule {

    @Binds
    abstract fun bindPrensenter(presenter: SquareRepoListPresenter): SquareRepoListContract.Presenter

    @Module
    companion object {

        @JvmStatic
        @PerSquareRepos
        @Provides
        fun provideSquareRepoListViewModelMapper(): SquareRepoListViewModelMapper {
            return SquareRepoListViewModelMapper()
        }

        @JvmStatic
        @PerSquareRepos
        @Provides
        fun provideSquareRepoListAdapter(context: Context): SquareRepoListAdapter {
            return SquareRepoListAdapter(context)
        }


    }
}

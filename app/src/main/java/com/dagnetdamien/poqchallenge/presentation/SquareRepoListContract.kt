package com.dagnetdamien.poqchallenge.presentation

interface SquareRepoListContract {

    interface View {
        fun showLoading()
        fun hideLoading()
        fun showError()
        fun showSquareRepoListInView(squareRepoListViewModel: SquareRepoListViewModel)
        fun openRepoUrlInBrowser(repoUrl: String)
    }

    interface Presenter : OnRepoClickListener {
        fun setView(view: View)
        fun initialise()
        fun cleanup()
    }
}

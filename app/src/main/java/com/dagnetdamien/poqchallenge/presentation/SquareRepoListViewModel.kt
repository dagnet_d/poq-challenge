package com.dagnetdamien.poqchallenge.presentation

import android.view.View

data class SquareRepoListViewModel(
    val header: Header,
    val repoList: List<Item>
) {

    data class Header(
        val title: String = "",
        val imageUrl: String = "",
        val visibility: Int = View.GONE
    )

    data class Item(
        val name: String,
        val description: String,
        val stars: String,
        val watchers: String,
        val forks: String,
        val url: String,
        val ownerName: String,
        val ownerImageUrl: String,
        val licenseName: String
    )
}

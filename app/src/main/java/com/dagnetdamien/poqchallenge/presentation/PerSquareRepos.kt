package com.dagnetdamien.poqchallenge.presentation

import javax.inject.Scope

@Scope
@Retention
annotation class PerSquareRepos
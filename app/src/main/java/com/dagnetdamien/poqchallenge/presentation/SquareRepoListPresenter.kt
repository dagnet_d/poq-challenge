package com.dagnetdamien.poqchallenge.presentation

import android.util.Log
import androidx.annotation.VisibleForTesting
import com.dagnetdamien.poqchallenge.domain.GetSquareGithubReposUseCase
import com.dagnetdamien.poqchallenge.util.Mockable
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

@PerSquareRepos
@Mockable
class SquareRepoListPresenter
@Inject
constructor(
    private val getSquareGithubReposUseCase: GetSquareGithubReposUseCase,
    private val squareRepoListViewModelMapper: SquareRepoListViewModelMapper
) :
    SquareRepoListContract.Presenter, CoroutineScope {

    var job = SupervisorJob()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private var view: SquareRepoListContract.View? = null

    override fun setView(view: SquareRepoListContract.View) {
        this.view = view
    }

    override fun initialise() {
        launch(CoroutineExceptionHandler(handler = { _, throwable ->
            this.handleError(throwable)
            view?.hideLoading()
        })) {
            view?.showLoading()
            getSquareGithubReposUseCase.get()
                .run { squareRepoListViewModelMapper.toSquareRepoListViewModel(this) }
                .run { view?.showSquareRepoListInView(this) }
            view?.hideLoading()
        }
    }

    private fun handleError(throwable: Throwable) {
        view?.showError()
        this.logError(throwable)
    }

    @VisibleForTesting
    internal fun logError(throwable: Throwable) {
        Log.w(this.javaClass.simpleName, throwable)
    }

    override fun onRepoClicked(viewModelItem: SquareRepoListViewModel.Item) {
        view?.openRepoUrlInBrowser(viewModelItem.url)
    }

    override fun cleanup() {
        job.takeUnless { it.isCancelled }?.cancel()
        view = null
    }
}

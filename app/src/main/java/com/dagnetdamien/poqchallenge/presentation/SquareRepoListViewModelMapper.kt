package com.dagnetdamien.poqchallenge.presentation

import android.view.View
import com.dagnetdamien.poqchallenge.domain.GithubRepoModel
import com.dagnetdamien.poqchallenge.util.Mockable

@PerSquareRepos
@Mockable
class SquareRepoListViewModelMapper {

    fun toSquareRepoListViewModel(githubRepoModels: List<GithubRepoModel>): SquareRepoListViewModel {
        return SquareRepoListViewModel(
            header = mapHeaderViewModel(githubRepoModels),
            repoList = mapListItemViewModel(githubRepoModels)
        )
    }

    private fun mapHeaderViewModel(githubRepoModels: List<GithubRepoModel>): SquareRepoListViewModel.Header {
        return githubRepoModels
            .firstOrNull { it.ownerImageUrl.isNotEmpty() && it.ownerName.isNotEmpty() }
            ?.let {
                SquareRepoListViewModel.Header(
                    title = it.ownerName,
                    imageUrl = it.ownerImageUrl,
                    visibility = View.VISIBLE
                )
            } ?: SquareRepoListViewModel.Header()
    }

    private fun mapListItemViewModel(githubRepoModels: List<GithubRepoModel>): List<SquareRepoListViewModel.Item> {
        return githubRepoModels.map {
            SquareRepoListViewModel.Item(
                name = it.name,
                description = it.description,
                stars = it.stargazersCount.toString(),
                watchers = it.watchersCount.toString(),
                forks = it.forksCount.toString(),
                url = it.url,
                ownerName = it.ownerName,
                ownerImageUrl = it.ownerImageUrl,
                licenseName = it.licenseName
            )
        }
    }
}

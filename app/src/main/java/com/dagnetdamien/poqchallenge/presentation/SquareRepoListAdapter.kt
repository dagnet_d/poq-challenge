package com.dagnetdamien.poqchallenge.presentation

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.dagnetdamien.poqchallenge.R
import com.dagnetdamien.poqchallenge.databinding.ReposListItemBinding
import javax.inject.Inject

@PerSquareRepos
class SquareRepoListAdapter
@Inject
constructor(private val context: Context) :
    RecyclerView.Adapter<SquareRepoListAdapter.ViewHolder>() {

    private val viewModelItems: ArrayList<SquareRepoListViewModel.Item> = ArrayList()
    var listener: OnRepoClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ReposListItemBinding>(
            LayoutInflater.from(context), R.layout.repos_list_item, parent, false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val viewModel = viewModelItems[position]
        holder.binding.viewModel = viewModel
        holder.binding.root.setOnClickListener { listener?.onRepoClicked(viewModel) }
        holder.binding.executePendingBindings()
    }

    fun updateItems(viewModels: Collection<SquareRepoListViewModel.Item>) {
        this.viewModelItems.clear()
        this.viewModelItems.addAll(viewModels)
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return viewModelItems.size
    }

    inner class ViewHolder(var binding: ReposListItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}

interface OnRepoClickListener {
    fun onRepoClicked(viewModelItem: SquareRepoListViewModel.Item)
}

package com.dagnetdamien.poqchallenge.presentation

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import com.dagnetdamien.poqchallenge.R
import com.dagnetdamien.poqchallenge.databinding.ActivityMainBinding
import com.dagnetdamien.poqchallenge.util.CustomChromeTab
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), SquareRepoListContract.View {

    @Inject
    lateinit var presenter: SquareRepoListContract.Presenter

    @Inject
    lateinit var adapter: SquareRepoListAdapter

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(toolbar)

        adapter.listener = presenter
        list.adapter = adapter
        list.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        presenter.setView(this)
        presenter.initialise()
    }

    override fun onDestroy() {
        presenter.cleanup()
        super.onDestroy()
    }

    override fun showLoading() {
        header_main.visibility = View.GONE
        list.visibility = View.GONE
        progress.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progress.visibility = View.GONE
    }

    override fun showError() {
        Snackbar.make(
            coordinatorLayout,
            getString(R.string.error_message),
            Snackbar.LENGTH_INDEFINITE
        ).show()
    }

    override fun showSquareRepoListInView(squareRepoListViewModel: SquareRepoListViewModel) {
        list.visibility = View.VISIBLE
        adapter.updateItems(squareRepoListViewModel.repoList)
        binding.viewModel = squareRepoListViewModel
        binding.executePendingBindings()
    }

    override fun openRepoUrlInBrowser(repoUrl: String) {
        CustomChromeTab.launchWithFallback(this, repoUrl)
    }
}

package com.dagnetdamien.poqchallenge.presentation

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @PerSquareRepos
    @ContributesAndroidInjector(modules = [SquareRepoPresentationModule::class])
    internal abstract fun contributeMainActivityInjector(): MainActivity
}
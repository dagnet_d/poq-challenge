package com.dagnetdamien.poqchallenge.di

import com.dagnetdamien.poqchallenge.data.GithubReposRepositoryImpl
import com.dagnetdamien.poqchallenge.data.LocalGithubReposDataStore
import com.dagnetdamien.poqchallenge.data.NetworkGithubReposDataStore
import com.dagnetdamien.poqchallenge.domain.GetSquareGithubReposUseCase
import com.dagnetdamien.poqchallenge.domain.GetTopPopularSquareReposUseCaseImpl
import com.dagnetdamien.poqchallenge.domain.GithubReposRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SquareRepoModule {

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun provideGithubReposRepository(
            localGithubReposDataStore: LocalGithubReposDataStore,
            networkGithubReposDataStore: NetworkGithubReposDataStore
        ): GithubReposRepository {
            return GithubReposRepositoryImpl(localGithubReposDataStore, networkGithubReposDataStore)
        }

        @Singleton
        @JvmStatic
        @Provides
        fun provideGetSquareGithubReposUseCase(githubReposRepository: GithubReposRepository)
                : GetSquareGithubReposUseCase {
            return GetTopPopularSquareReposUseCaseImpl(githubReposRepository)
        }
    }
}
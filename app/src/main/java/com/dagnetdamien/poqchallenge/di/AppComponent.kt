package com.dagnetdamien.poqchallenge.di

import com.dagnetdamien.poqchallenge.PoqChallengeApplication
import com.dagnetdamien.poqchallenge.data.database.DatabaseModule
import com.dagnetdamien.poqchallenge.data.network.NetworkModule
import com.dagnetdamien.poqchallenge.presentation.ActivityModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidInjectionModule::class,
        AppModule::class,
        ActivityModule::class,
        NetworkModule::class,
        DatabaseModule::class,
        SquareRepoModule::class
    ]
)
interface AppComponent {
    fun inject(poqChallengeApplication: PoqChallengeApplication)
}
package com.dagnetdamien.poqchallenge.data.network

import com.dagnetdamien.poqchallenge.data.NetworkGithubReposDataStore
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
abstract class NetworkModule {

    @Module
    companion object {
        private const val ENDPOINT = "https://api.github.com"

        @JvmStatic
        @Singleton
        @Provides
        fun provideGson(): Gson {
            return GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setLenient()
                .create()
        }

        @JvmStatic
        @Singleton
        @Provides
        fun provideOkHttpClient(): OkHttpClient {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val builder = OkHttpClient.Builder()
            builder.addInterceptor(loggingInterceptor)
            return builder.build()
        }

        @JvmStatic
        @Singleton
        @Provides
        fun provideCoroutinesErrorCallAdapterFactory(): CoroutineCallAdapterFactory {
            return CoroutineCallAdapterFactory()
        }

        @JvmStatic
        @Singleton
        @Provides
        fun provideRetrofit(
            gson: Gson,
            coroutineCallAdapterFactory: CoroutineCallAdapterFactory,
            okHttpClient: OkHttpClient
        ): Retrofit {
            return Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addCallAdapterFactory(coroutineCallAdapterFactory)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()
        }

        @JvmStatic
        @Singleton
        @Provides
        fun provideGithubApi(retrofit: Retrofit): GithubApi {
            return retrofit.create<GithubApi>(GithubApi::class.java)
        }

        @JvmStatic
        @Provides
        @Singleton
        fun provideNetworkGithubReposDataStore(githubApi: GithubApi): NetworkGithubReposDataStore {
            return NetworkGithubReposDataStoreImpl(githubApi)
        }
    }

}

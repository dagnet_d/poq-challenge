package com.dagnetdamien.poqchallenge.data.database

import android.app.Application
import androidx.room.Room
import com.dagnetdamien.poqchallenge.data.LocalGithubReposDataStore
import com.dagnetdamien.poqchallenge.data.database.AppDatabase.Companion.DATABASE_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Module
    companion object {

        @JvmStatic
        @Provides
        @Singleton
        fun provideAppDatabase(application: Application): AppDatabase {
            return Room
                .databaseBuilder(application, AppDatabase::class.java, DATABASE_NAME)
                .build()
        }

        @JvmStatic
        @Provides
        @Singleton
        fun provideLocalGithubReposDataStore(appDatabase: AppDatabase): LocalGithubReposDataStore {
            return LocalGithubReposDataStoreImpl(appDatabase)
        }
    }
}
package com.dagnetdamien.poqchallenge.data.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.dagnetdamien.poqchallenge.data.database.GithubRepo.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class GithubRepo(
    @PrimaryKey val id: String,
    @ColumnInfo val name: String = "",
    @ColumnInfo val description: String = "",
    @ColumnInfo val stargazersCount: Int = 0,
    @ColumnInfo val watchersCount: Int = 0,
    @ColumnInfo val forksCount: Int = 0,
    @ColumnInfo val url: String = "",
    @ColumnInfo val isPrivate: Boolean = false,
    @ColumnInfo val hasIssues: Boolean = false,
    @ColumnInfo val hasWiki: Boolean = false,
    @ColumnInfo val createdAt: String = "",
    @ColumnInfo val ownerName: String = "",
    @ColumnInfo val ownerImageUrl: String = "",
    @ColumnInfo val licenseName: String = ""
) {
    companion object {
        const val TABLE_NAME = "github_repo"
    }
}

package com.dagnetdamien.poqchallenge.data

import com.dagnetdamien.poqchallenge.data.database.GithubRepo
import com.dagnetdamien.poqchallenge.data.network.GithubRepoResponse
import com.dagnetdamien.poqchallenge.domain.GithubReposRepository

class GithubReposRepositoryImpl(
    private val localGithubReposDataStore: LocalGithubReposDataStore,
    private val networkGithubReposDataStore: NetworkGithubReposDataStore
) : GithubReposRepository {

    override suspend fun squareRepos(): List<GithubRepo> {
        return getSquareReposCache()
            ?: fetchSquareRepos()
            ?: throw SquareReposNotFoundException()
    }

    private suspend fun getSquareReposCache(): List<GithubRepo>? {
        return localGithubReposDataStore
            .findAllSquareRepos()
            .takeIf { isCacheValid(it) }
    }

    private fun isCacheValid(githubRepos: List<GithubRepo>) = githubRepos.isNotEmpty()

    private suspend fun fetchSquareRepos(): List<GithubRepo>? {
        return networkGithubReposDataStore
            .squareRepos()
            .mapNotNull { toGithubRepo(it) }
            .takeIf { it.isNotEmpty() }
            ?.let {
                localGithubReposDataStore.saveAll(it)
                it
            }
    }

    private fun toGithubRepo(githubRepoResponse: GithubRepoResponse?): GithubRepo? {
        return githubRepoResponse
            ?.let {
                GithubRepo(
                    id = it.id ?: "",
                    name = it.name ?: "",
                    description = it.description ?: "",
                    stargazersCount = it.stargazersCount ?: 0,
                    watchersCount = it.watchersCount ?: 0,
                    forksCount = it.forksCount ?: 0,
                    url = it.htmlUrl ?: "",
                    isPrivate = it.private ?: false,
                    hasIssues = it.hasIssues ?: false,
                    hasWiki = it.hasWiki ?: false,
                    createdAt = it.createdAt ?: "",
                    ownerName = it.owner?.login ?: "",
                    ownerImageUrl = it.owner?.avatarUrl ?: "",
                    licenseName = it.license?.name ?: ""
                )
            }
            ?.takeIf { it.id.isNotEmpty() }
    }
}

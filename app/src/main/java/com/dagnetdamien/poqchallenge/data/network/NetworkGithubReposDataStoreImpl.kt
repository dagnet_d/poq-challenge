package com.dagnetdamien.poqchallenge.data.network

import com.dagnetdamien.poqchallenge.data.NetworkGithubReposDataStore
import javax.inject.Inject

class NetworkGithubReposDataStoreImpl
@Inject
constructor(private val githubApi: GithubApi) : NetworkGithubReposDataStore {

    override suspend fun squareRepos(): List<GithubRepoResponse?> {
        return githubApi.squareRepos().await()
    }
}

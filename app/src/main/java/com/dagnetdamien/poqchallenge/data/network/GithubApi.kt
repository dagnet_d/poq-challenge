package com.dagnetdamien.poqchallenge.data.network

import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface GithubApi {

    @GET("/orgs/square/repos")
    fun squareRepos(): Deferred<List<GithubRepoResponse>>
}

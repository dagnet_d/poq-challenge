package com.dagnetdamien.poqchallenge.data

import com.dagnetdamien.poqchallenge.data.database.GithubRepo

interface LocalGithubReposDataStore {

    suspend fun findAllSquareRepos(): List<GithubRepo>

    suspend fun saveAll(githubRepos: List<GithubRepo>)

}

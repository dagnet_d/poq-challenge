package com.dagnetdamien.poqchallenge.data.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [GithubRepo::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        const val DATABASE_NAME = "poq-challenge-db"
    }

    abstract fun githubRepoDao(): GithubRepoDao
}
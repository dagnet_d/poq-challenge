package com.dagnetdamien.poqchallenge.data.database

import com.dagnetdamien.poqchallenge.data.LocalGithubReposDataStore
import javax.inject.Inject

class LocalGithubReposDataStoreImpl
@Inject
constructor(private val appDatabase: AppDatabase) : LocalGithubReposDataStore {

    override suspend fun findAllSquareRepos() = appDatabase.githubRepoDao().getAll()

    override suspend fun saveAll(githubRepos: List<GithubRepo>) {
        appDatabase.githubRepoDao().insertAll(*githubRepos.toTypedArray())
    }
}

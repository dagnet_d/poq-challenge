package com.dagnetdamien.poqchallenge.data

import com.dagnetdamien.poqchallenge.data.network.GithubRepoResponse

interface NetworkGithubReposDataStore {

    suspend fun squareRepos(): List<GithubRepoResponse?>
}
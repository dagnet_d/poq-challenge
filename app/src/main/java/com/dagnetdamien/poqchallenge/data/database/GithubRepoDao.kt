package com.dagnetdamien.poqchallenge.data.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.dagnetdamien.poqchallenge.data.database.GithubRepo.Companion.TABLE_NAME

@Dao
interface GithubRepoDao {

    @Query("SELECT * FROM $TABLE_NAME")
    suspend fun getAll(): List<GithubRepo>

    @Insert
    suspend fun insertAll(vararg githubRepo: GithubRepo)

    @Delete
    suspend fun delete(githubRepo: GithubRepo)
}

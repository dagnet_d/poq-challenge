package com.dagnetdamien.poqchallenge.data.network

data class GithubRepoResponse(
    val id: String? = "",
    val name: String? = "",
    val description: String? = "",
    val stargazersCount: Int? = 0,
    val watchersCount: Int? = 0,
    val forksCount: Int? = 0,
    val htmlUrl: String? = null,
    val private: Boolean? = false,
    val hasIssues: Boolean? = false,
    val hasWiki: Boolean? = false,
    val createdAt: String? = null,
    val owner: RepoOwnerResponse? = null,
    val license: RepoLicenceResponse? = null
) {
    data class RepoOwnerResponse(val avatarUrl: String?, val login: String?)
    data class RepoLicenceResponse(val name: String?)
}

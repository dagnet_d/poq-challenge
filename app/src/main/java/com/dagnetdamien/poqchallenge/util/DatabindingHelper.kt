package com.dagnetdamien.poqchallenge.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

class DatabindingHelper {

    companion object {

        @JvmStatic
        @BindingAdapter("bind:imgUrl")
        fun setImageSource(imageView: ImageView, src: String?) {
            if (src == null || src.isEmpty()) {
                return
            }
            Picasso.get()
                .load(src)
                .fit().centerInside()
                .into(imageView)
        }
    }

}

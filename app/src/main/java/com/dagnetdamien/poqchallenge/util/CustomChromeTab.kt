package com.dagnetdamien.poqchallenge.util

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import androidx.browser.customtabs.CustomTabsService
import androidx.core.content.ContextCompat
import com.dagnetdamien.poqchallenge.R

class CustomChromeTab {

    companion object {

        fun launchWithFallback(context: Context, url: String) {
            val uri = Uri.parse(url)

            if (supportsCustomTabs(context, uri)) {
                createIntent(context).launchUrl(context, uri)
            } else try {
                context.startActivity(Intent(Intent.ACTION_VIEW, uri))
            } catch (exception: ActivityNotFoundException) {
                Log.w("CustomChromeTab", exception)
                Toast.makeText(context, R.string.error_no_apps_to_handle_intent, Toast.LENGTH_SHORT)
                    .show()
            }
        }

        fun createIntent(context: Context): CustomTabsIntent {
            return CustomTabsIntent.Builder().setShowTitle(true)
                .setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .build()
        }

        fun supportsCustomTabs(context: Context, uri: Uri): Boolean {
            val actionViewIntent = Intent(Intent.ACTION_VIEW, uri)
            val packageManager = context.packageManager
            val resolvedActivities = packageManager.queryIntentActivities(actionViewIntent, 0)

            for (info in resolvedActivities) {
                val serviceIntent = Intent()
                serviceIntent.action = CustomTabsService.ACTION_CUSTOM_TABS_CONNECTION
                serviceIntent.`package` = info.activityInfo.packageName
                if (packageManager.resolveService(serviceIntent, 0) != null) {
                    return true
                }
            }
            return false
        }
    }
}

package com.dagnetdamien.poqchallenge.domain

import androidx.annotation.VisibleForTesting
import com.dagnetdamien.poqchallenge.data.database.GithubRepo
import com.dagnetdamien.poqchallenge.util.Mockable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

@Mockable
class GetTopPopularSquareReposUseCaseImpl
@Inject
constructor(private val githubReposRepository: GithubReposRepository) :
    GetSquareGithubReposUseCase {

    companion object {
        const val TOP_LIMIT = 15
    }

    override suspend fun get(): List<GithubRepoModel> {
        return withContext(Dispatchers.Default) {
            githubReposRepository
                .squareRepos()
                .sortedByDescending { it.watchersCount + it.stargazersCount + it.forksCount }
                .take(topLimit())
                .map { toGithubRepoModel(it) }
        }
    }

    private fun toGithubRepoModel(githubRepo: GithubRepo) =
        GithubRepoModel(
            name = githubRepo.name,
            description = githubRepo.description,
            stargazersCount = githubRepo.stargazersCount,
            watchersCount = githubRepo.watchersCount,
            forksCount = githubRepo.forksCount,
            url = githubRepo.url,
            isPrivate = githubRepo.isPrivate,
            hasIssues = githubRepo.hasIssues,
            hasWiki = githubRepo.hasWiki,
            ownerName = githubRepo.ownerName,
            ownerImageUrl = githubRepo.ownerImageUrl,
            licenseName = githubRepo.licenseName
        )

    @VisibleForTesting
    internal fun topLimit() = TOP_LIMIT
}

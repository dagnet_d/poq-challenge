package com.dagnetdamien.poqchallenge.domain

data class GithubRepoModel(
    val name: String = "",
    val description: String = "",
    val stargazersCount: Int = 0,
    val watchersCount: Int = 0,
    val forksCount: Int = 0,
    val url: String = "",
    val isPrivate: Boolean = false,
    val hasIssues: Boolean = false,
    val hasWiki: Boolean = false,
    val ownerName: String = "",
    val ownerImageUrl: String = "",
    val licenseName: String = ""
)

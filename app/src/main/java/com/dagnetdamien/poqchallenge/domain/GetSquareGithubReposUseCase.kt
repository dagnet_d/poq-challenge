package com.dagnetdamien.poqchallenge.domain

interface GetSquareGithubReposUseCase {

    suspend fun get(): List<GithubRepoModel>
}
package com.dagnetdamien.poqchallenge.domain

import com.dagnetdamien.poqchallenge.data.database.GithubRepo

interface GithubReposRepository {

    suspend fun squareRepos(): List<GithubRepo>
}